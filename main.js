//ENIGMA MACHINE DEFAULT CONFIGURATIONS
let characherArray = "QWERTYUIOPASDFGHJKLZXCVBNM";
let rotor1state = 25,
    rotor2state = 26,
    rotor3state = 16,
    rotor1Config = "EKMFLGDQVZNTOWYHXUSPAIBRCJ",    //Enigma-1 (1930)
    rotor2Config = "AJDKSIRUXBLHWTMCQGZNPYFVOE",    //Enigma-1 (1930)
    rotor3Config = "BDFHJLCPRTXVZNYEIWGAKMUSQO",    //Enigma-1 (1930)
    rotor4config = "ESOVPZJAYQUIRHXLNFTGKDCMWB",    //M3-ARMY (1938)
    rotor5config = "VZBRGITYUPSDNHLXAWMJQOFECK",    //M3-ARMY (1938)
    reflectorConfig = "EJMZALYXVBWFCRQUONTSPIKHGD";
    plugboard = {                           //at most 10 configurations for plugboard
        "a" : "t",
        "b" : "z",
        "c" : "k",
        "d" : "y",
        "r" : "w",
        "e" : "x",
        "f" : "m",
        "g" : "n",
        "h" : "o",
        "i" : "p",
    };
let pressedKey = null;      //to prevent 2 key presses at a time
//the configuration of machine is defined by these variables
let enigmaOutput = "",
    enigmaInput = "",
    rotor1 = new Array(),
    rotor2 =new Array(),
    rotor3 = new Array(),
    reflector = new Array();

currentConfig = btoa('{"plugboard":' + JSON.stringify(plugboard) + ',"rotor1state":' + rotor1state + ', "rotor2state":' + rotor2state +', "rotor3state":' + rotor3state + '}');
initializeWithConfig(currentConfig);

//display alphabets to light up while typing
for (let i = 0; i < 26; i++) {
    if(i===0 || i===9 || i===17){
        var mainDiv = document.createElement("div");
        mainDiv.classList.add("row");
        document.querySelector(".writing-board").appendChild(mainDiv);
    }
    let div = document.createElement("div");
    let char = characherArray.charAt(i);
    div.innerHTML = char;
    div.classList.add("button");
    div.id = char;
    mainDiv.appendChild(div);
}

function initializeWithConfig(config){
    conf = JSON.parse(atob(config));
    plugboard = conf.plugboard;
    rotor1state = conf.rotor1state;
    rotor2state = conf.rotor2state;
    rotor3state = conf.rotor3state;

    document.querySelector("#text-to-be-copied").innerHTML = config;

    enigmaInput = "";
    enigmaOutput = "";
    document.querySelector(".output").innerHTML = enigmaOutput;
    document.querySelector(".input").innerHTML = enigmaInput;

    changeAlphabetsToConfig(rotor1Config, rotor2Config, rotor3Config, reflectorConfig);     //Set the rotor configurations with 3 rotors and a reflector
    updateRotor();          //display current rotor state in html page
    rotateWheelsInitial();
}

//button to copy current configuration
document.getElementById("copy-this-config").addEventListener('click', function(){
    let el =document.getElementById("text-to-be-copied");

    el.select();                                    // Select the <textarea> content
    document.execCommand('copy');                   // Copy - only works as a result of a user action (e.g. click events)

});

//on key down input key to the machine
document.addEventListener('keydown', function(e){
    let char = e.key;
    let letters = /^[A-Za-z]$/;
    char = char.toLowerCase();

    //check if keyboard press is an alphabet and no other key is already pressed
    if(char.match(letters) && pressedKey === null){
        typed(char);
    }

});
for(i=0; enigBtn= document.querySelectorAll(".writing-board .button")[i]; i++){

    enigBtn.addEventListener("mousedown", function () {
        let char = this.id;
        let letters = /^[A-Za-z]$/;
        char = char.toLowerCase();

        //check if keyboard press is an alphabet and no other key is already pressed
        if(char.match(letters) && pressedKey === null){
            typed(char);
        }
    });

    //on mouse up remove pressed key and the lighting in the key
    enigBtn.addEventListener('mouseup', function(){
        let char = this.id;
        if(char === pressedKey.toUpperCase()) {
            let elementId = "#" + char;
            document.querySelector(elementId).classList.remove("active");
            pressedKey = null;
        }
    });

}

//on key up remove pressed key and the lighting in the key
document.addEventListener('keyup', function(e){
    let char = e.key;
    let letters = /^[A-Za-z]$/;
        if(char.match(letters) && pressedKey !== null && char.toLowerCase() === pressedKey.toLowerCase()) {
            let elementId = "#" + char.toUpperCase();
            document.querySelector(elementId).classList.remove("active");
            pressedKey = null;
        }
});


//handle each input
function typed(letter){
    //indicate the key press and highlight the key
    pressedKey = letter;
    let elementId = "#" + letter.toUpperCase();
    document.querySelector(elementId).classList.add("active");

    let plug = getFromPlugboard(letter),
        out1 = reflector[rotor1[rotor2[rotor3[plug]]]],     //pass the output of each step to next rotor
        out2 = getKeyFromValue(1, out1),           //reverse the process and get key from value for reflected input
        out3 = getKeyFromValue(2, out2),
        out = getFromPlugboard(getKeyFromValue(3, out3));        //output is the final output from machine

    //append the result to an array and display it
    enigmaOutput += out;
    enigmaInput += letter;
    document.querySelector(".output").innerHTML = enigmaOutput;
    document.querySelector(".input").innerHTML = enigmaInput;


    //update the rotor configs in html and rotate it
    rotateRotor();
    updateRotor();
}

//Function to rotate the third rotor (first one from right) once
function rotateRotor(){
    first = rotor3["a"];
    for(let i=0; i<25; i++){
        let char = (i+10).toString(36);
        let charNext = (i+11).toString(36);
        rotor3[char] = rotor3[charNext];
    }
    rotor3["z"] = first;
    if(rotor3state === 26){
        rotor3state = 1;
        rotateRotor2();
    }else {
        rotor3state++;
    }
}

//Function to rotate the second rotor (middle one) once
function rotateRotor2(){
    first = rotor2["a"];
    for(let i=0; i<25; i++){
        let char = (i+10).toString(36);
        let charNext = (i+11).toString(36);
        rotor2[char] = rotor2[charNext];
    }
    rotor2["z"] = first;
    if(rotor2state === 26){
        rotor2state = 1;
        rotateRotor1();
    }else {
        rotor2state++;
    }
}

//Function to rotate the first rotor (first one from left) once
function rotateRotor1(){
    first = rotor1["a"];
    for(let i=0; i<25; i++){
        let char = (i+10).toString(36);
        let charNext = (i+11).toString(36);
        rotor1[char] = rotor1[charNext];
    }
    rotor1["z"] = first;
    if(rotor1state === 26){
        rotor1state = 1;
    }else {
        rotor1state++;
    }
}

//Update the rotor state in html page
function updateRotor(){
    let elemRotor1 = document.querySelector(".rotor#one"),
        elemRotor2 = document.querySelector(".rotor#two"),
        elemRotor3 = document.querySelector(".rotor#three");

    elemRotor1.innerHTML = rotor1state;
    elemRotor2.innerHTML = rotor2state;
    elemRotor3.innerHTML = rotor3state;
}

//Initialize rotors with given configurations
function changeAlphabetsToConfig(rot1, rot2, rot3, ref){
    rot1 = rot1.toLowerCase();
    rot2 = rot2.toLowerCase();
    rot3 = rot3.toLowerCase();
    ref = ref.toLowerCase();
    for(let i=0; i<26; i++){
        let char = (i+10).toString(36);
        rotor1[char] = rot1.charAt(i);
        rotor2[char] = rot2.charAt(i);
        rotor3[char] = rot3.charAt(i);
        reflector[char] = ref.charAt(i);
    }
}

//Rotate the wheels to given rotor configurations
function rotateWheelsInitial(){
    let a, b, c;
    for(c = rotor1state; c>1; c--){
        rotateRotor1();
        rotor1state--;
    }
    for(b = rotor2state; b>1; b--){
        rotateRotor2();
        rotor2state--;
    }
    for(a = rotor3state; a>1; a--){
        rotateRotor();
        rotor3state--;
    }
    if(rotor1state === 0) rotor1state++;
    if(rotor2state === 0) rotor2state++;
    if(rotor3state === 0) rotor3state++;
}

//To get key from value of an associative array
function getKeyFromValue(n, value){
    switch(n){
        case 1: arr = rotor1;
            break;
        case 2: arr = rotor2;
            break;
        case 3: arr = rotor3;
            break;
    }
    for(i=0; i<26; i++){
        let char = (i+10).toString(36);
        if(arr[char] === value){
            return char;
        }
    }
}

//plug board output
function getFromPlugboard(letter){
    if(plugboard[letter]){
        return plugboard[letter];
    }else{
        for(i=0; i<26; i++) {
            let char = (i + 10).toString(36);
            if (plugboard[char] && plugboard[char] === letter) {
                return char;
            }
        }
        return letter;
    }
}

function setRandomConfiguration(){
    a = Math.floor(1+Math.random()*25.9);
    b = Math.floor(1+Math.random()*25.9);
    c = Math.floor(1+Math.random()*25.9);
    let arr = [];
    numberOfPlugs = Math.floor(1+Math.random()*9.9) * 2;
    for(i = 0;  i < numberOfPlugs; i++){
        do {
            newElem =  Math.floor(1+Math.random()*25.9);
            rpt = false;
            for (j = 0; j < i; j++) {
                if (arr[j] === newElem) {
                    rpt = true;
                }
            }
        }while(rpt);
        arr.push(newElem);
    }
    newPlugboard = [];
    for(i=0; i<numberOfPlugs; i=i+2){
        newPlugboard[String.fromCharCode(arr[i] + 96)] = String.fromCharCode(arr[i+1] + 96);
    }


    currentConfig = btoa('{"plugboard":' + JSON.stringify(newPlugboard) + ',"rotor1state":' + a + ', "rotor2state":' + b +', "rotor3state":' + c + '}');
    initializeWithConfig(currentConfig);
}
